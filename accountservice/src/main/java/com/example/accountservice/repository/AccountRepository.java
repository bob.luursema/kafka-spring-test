package com.example.accountservice.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.accountservice.Account;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {
}