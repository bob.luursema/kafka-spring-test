package com.example.accountservice;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Account {
	@GeneratedValue
	@Id
	private long id;
	private String name;
	private boolean customerVerified;
}
