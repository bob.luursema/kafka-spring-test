package com.example.accountservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.accountservice.Account;
import com.example.accountservice.AccountService;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
public class Controller {
	@Autowired
	private AccountService service;

// get the message as a complex type via HTTP, publish it to broker using spring cloud stream
	@RequestMapping(value = "/sendMessage/complexType", method = RequestMethod.POST)
	public String publishMessageComplextType(@RequestBody Account account) throws JsonProcessingException {
		service.createAccount(account);
		return "success";
	}
}