# Install Kafka

Download Kafka from https://kafka.apache.org/downloads.
Unzip the .tgz and unzip the .tar

In `config/zookeeper.properties` change `dataDir` to a local folder.

In `config/server.properties` change `log.dirs` to local folder.

Start `.\bin\windows\zookeeper-server-start.bat config/zookeeper.properties`

Start `.\bin\windows\kafka-server-start.bat config/server.properties`
