package com.example.customer.check.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerCheckApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerCheckApplication.class, args);
	}

}
